var CURRENTRECORDMODULE, FORMATMODULE, URLMODULE, HTTPSMODULE, UIDIALOGMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * Script to deploy on customrecord_bwp_pricingrule custom record
 */
define(['N/currentRecord', 'N/format', 'N/url', 'N/https', 'N/ui/dialog'], runClient);

function runClient(currentRecord, format, url, https, uidialog) {
	CURRENTRECORDMODULE= currentRecord;
	FORMATMODULE= format;
	URLMODULE= url;
	HTTPSMODULE= https;
	UIDIALOGMODULE= uidialog;
	
	let returnObj = {};
	returnObj['pageInit'] = _pageInit;
	// returnObj['fieldChanged'] = _fieldChanged;
	// returnObj['postSourcing'] = _postSourcing;
	// returnObj['sublistChanged'] = _sublistChanged;
	// returnObj['lineInit'] = _lineInit;
	// returnObj['validateField'] = _validateField;
	// returnObj['validateLine'] = _validateLine;
	// returnObj['validateInsert'] = _validateInsert;
	// returnObj['validateDelete'] = _validateDelete;
	// returnObj['saveRecord'] = _saveRecord;
	returnObj['printPackingList'] = printPackingListCall;
	returnObj['printDeliveryNote'] = printDeliveryNoteCall;
	return returnObj;
}

/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {

}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {

}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {

}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {

}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {
	return true;
}

function printPackingListCall() {
	let currentRec = CURRENTRECORDMODULE.get();
	let itemFulfill = currentRec.id;
	
	//get the url for the suitelet - add woRecordId parameter
	let suiteletURL = URLMODULE.resolveScript({
		scriptId: 'customscript_bwp_sl_printpackinglist',
		deploymentId: 'customdeploy_bwp_sl_printpackinglist',
		params: {custparam_bwp_itemfulfillment: itemFulfill},
		returnExternalUrl: false
	});
	
	window.open(suiteletURL);
}

function printDeliveryNoteCall() {
	let currentRec = CURRENTRECORDMODULE.get();
	let itemFulfill = currentRec.id;
	
	//get the url for the suitelet - add woRecordId parameter
	let suiteletURL = URLMODULE.resolveScript({
		scriptId: 'customscript_bwp_sl_printdeliverynote',
		deploymentId: 'customdeploy_bwp_sl_printdeliverynote',
		params: {custparam_bwp_itemfulfillment: itemFulfill},
		returnExternalUrl: false
	});
	
	window.open(suiteletURL);
}
