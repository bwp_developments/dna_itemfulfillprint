var RENDERMODULE, RUNTIMEMODULE, SEARCHMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
 define(['N/render', 'N/runtime', 'N/search'], runSuitelet);

 function runSuitelet(render, runtime, search) {
     RENDERMODULE= render;
     RUNTIMEMODULE= runtime;
     SEARCHMODULE= search;
     
     var returnObj = {};
     returnObj['onRequest'] = _onRequest;
     return returnObj;	
 }
 
/**
 * Defines the Suitelet script trigger point.
 * @param {Object} scriptContext
 * @param {ServerRequest} scriptContext.request - Incoming request
 * @param {ServerResponse} scriptContext.response - Suitelet response
 * @since 2015.2
 */
 function _onRequest(context) {
    let method = context.request.method;
    //logRecord(context, 'context');

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
}

function getFunction(context) {
    const itemFulfill = context.request.parameters.custparam_bwp_itemfulfillment;
    const scriptObj = RUNTIMEMODULE.getCurrentScript();
    const formId = parseInt(scriptObj.getParameter({ name: 'custscript_bwp_packinglisttemplateformid' }), 10);
    logVar('formId', formId);
	let renderFile = RENDERMODULE.packingSlip({
        entityId: parseInt(itemFulfill, 10),
        printMode: RENDERMODULE.PrintMode.PDF,
        formId: formId,
        // formId: 142,
    });

    context.response.writeFile({
        file: renderFile,
        isInline: true
    });
}

function postFunction(context) {
    context.response.write('none found');
}

function logRecord(logTitle, record) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});
}