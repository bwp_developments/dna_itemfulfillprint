var RECORDMODULE, SEARCHMODULE, UISERVERWIDGETMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/ui/serverWidget'], runUserEvent);

function runUserEvent(record, search, serverWidget) {
	RECORDMODULE= record;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= serverWidget;
	
	let returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
	// returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}


/**
 * Defines the function definition that is executed before record is loaded.
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
 * @param {Form} scriptContext.form - Current form
 * @param {ServletRequest} scriptContext.request - HTTP request information sent from the browser for a client action only.
 * @since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');

    if (scriptContext.type == scriptContext.UserEventType.VIEW && scriptContext.request) {
        scriptContext.form.clientScriptModulePath = './bwp_cs_itemfulfillmentprint.js';
        let packingListButton = scriptContext.form.addButton({
            id: 'custpage_bwp_printpackinglistbutton',
            label: 'Packing List',
            functionName: 'printPackingList'
        });
        let deliveryNoteButton = scriptContext.form.addButton({
            id: 'custpage_bwp_printdeliverynotebutton',
            label: 'Delivery Note',
            functionName: 'printDeliveryNote'
        });
    }
	
	log.debug('beforeLoad done');
}

/**
 * Defines the function definition that is executed before record is submitted.
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
 * @since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');

	let itemFulfillRecord = scriptContext.newRecord;
	const lineCount = itemFulfillRecord.getLineCount({ sublistId: 'item' });
	for (let i = 0 ; i < lineCount ; i++) {
		let inventDetailRecord = itemFulfillRecord.getSublistSubrecord({
			sublistId: 'item',
			fieldId: 'inventorydetail',
			line: i
		});
		let inventDetailText = '';
		if (inventDetailRecord) {
			let inventDetailCount = inventDetailRecord.getLineCount({ sublistId: 'inventoryassignment' });
			for (let j = 0 ; j < inventDetailCount ; j++) {
				let inventoryNumberId = inventDetailRecord.getSublistValue({ 
					sublistId: 'inventoryassignment',
					fieldId: 'issueinventorynumber',
					line: j
				});
				if (!inventoryNumberId) {
					break;
				}
				let fieldsValues = SEARCHMODULE.lookupFields({
					type: SEARCHMODULE.Type.INVENTORY_NUMBER,
					id: inventoryNumberId,
					columns: ['inventorynumber']
				})
				let inventoryNumber;
				if ('inventorynumber' in fieldsValues) {
					inventoryNumber = fieldsValues['inventorynumber'];
				} 
				let quantity = inventDetailRecord.getSublistValue({ 
					sublistId: 'inventoryassignment',
					fieldId: 'quantity',
					line: j
				});
				let expirationDate = inventDetailRecord.getSublistValue({ 
					sublistId: 'inventoryassignment',
					fieldId: 'expirationdate',
					line: j
				});
				let inventoryLineText = `${inventoryNumber || ''} (${quantity}) ${formatDate(expirationDate)}`;
				if (inventDetailText) {
					inventDetailText += '\r\n';
				}
				inventDetailText += inventoryLineText;
			}
		}
		itemFulfillRecord.setSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_inventorydetail',
			line: i,
			value: inventDetailText
		});
	}

	
	log.debug('beforeSubmit done');
}

/**
 * Defines the function definition that is executed after record is submitted.
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
 * @since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('afterSubmit started');
		
	log.debug('afterSubmit done');
}

/**
 * 
 * @param {Object} dateObj is a Date object
 * @returns date as a YYYY/MM/DD string
 */
 function formatDate(dateObj) {
	if (!dateObj) return '';

	const regex = /^(\d+)-(\d+)-(\d+)/;
	let regexResult = regex.exec(dateObj.toISOString());
	return `${regexResult[1]}/${regexResult[2]}/${regexResult[3]}`;
}

function logRecord(logTitle, record) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});
}
